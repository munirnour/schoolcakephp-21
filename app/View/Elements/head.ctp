<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php

		echo $this->Html->meta('icon');

		echo $this->Html->css(array('bootstrap','school','school-reset','jquery-ui'));
		echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">';

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->Html->script('jquery-3.3.1.min');
		echo $this->Html->script('jquery-1.8.2');
		echo $this->Html->script('jquery-ui');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('jquery.mask');
		echo $this->Html->script('school');
		
	?>
</head>