<section class="menu">
	<div class="logo">
		<h1><i class="fa fa-rocket"></i></h1>
	</div>

	<div class="list-menu">
	    <ul class="menu-list">
	    	<li>
	    		<a href="#">
	    			<i class="fa fa-home"></i>
	    			Home
	    		</a>
	    	</li>
	    	<li>
	    		<a href=" <?php echo $this->Html->url(array("controller" => "courses","action" => "index"));  ?> ">
	    			<i class="fa fa-slideshare"></i>
	    			Cursos
	    		</a>
	    	</li>
	    	<li>
	    		<a href=" <?php echo $this->Html->url(array("controller" => "classGroups","action" => "index"));  ?> ">
	    			<i class="fa fa-slideshare"></i>
	    			Turmas
	    		</a>
	    	</li>
	    	<li>
	    		<a href=" <?php echo $this->Html->url(array("controller" => "typeEnrolments","action" => "index"));  ?> ">
	    			<i class="fa fa-credit-card"></i>
	    			Descontos
	    		</a>
	    	</li>
	    	<li>
	    		<a href=" <?php echo $this->Html->url(array("controller" => "users","action" => "index"));  ?> ">
	    			<i class="fa fa-user"></i>
	    			Usuários
	    		</a>
	    	</li>
	    	<li>
	    		<a href=" <?php echo $this->Html->url(array("controller" => "ClassGroupsUsers","action" => "index"));  ?> ">
	    			<i class="fa fa-user-plus"></i>
	    			Inscrições
	    		</a>
	    	</li>
	    </ul>
	</div>
</section> <!-- End menu -->