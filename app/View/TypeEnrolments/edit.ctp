<div class="row">
	<div class="col-md-12">
		<div class="content-din">
		    <div class="title-page">
		    	<h1 class="title-pg">Formulário de ediçao tipo de incriçao</h1>
		    </div>

		    <?php echo $this->Form->create('TypeEnrolment'); ?>
		        <?php echo $this->form->input('id', array('type'=>'hidden')); ?>
		    	<div class="class-md-12">
		    		<div class="form-group">
		    		    <?php echo $this->Form->input('name', array('label'=>'Nome', 'class'=>'form-control')); ?>
		    	    </div>
		    	</div>
		    	
		    	<div class="col-md-6">
		    		<div class="form-group">
		    		    <?php echo $this->Form->input('description', array('type'=>'textarea','label'=>'Descrição', 'class'=>'form-control','rows'=>3)); ?>
		    	    </div>
		    	</div>
		    	
		    	<div class="col-md-6">
		    	    <div class="row">
		    	    	<div class="col-md-6">
		    	    		<div class="form-group">
					    		<?php echo $this->Form->input('amount', array('label'=>'Porcentagem', 'class'=>array('form-control','price'),'type'=>'text')); ?>
					    	</div>
		    	    	</div>
		    	    	<div class="col-md-6">
		    	    		<div class="form-group">
					    		<?php echo $this->Form->input('discount', array('label'=>'Valor', 'class'=>array('form-control','price'),'type'=>'text')); ?>
					    	</div>
		    	    	</div>
		    	    </div>
		    	</div>
		    	
		    	
		    	
		    	<div class="row">
		    		<div class="col-md-12">
		    		<div class="form-group">
			    		<?php echo $this->Form->button('Salvar', array('type' => 'submit', 'class'=>'btn-salvar')); ?>
			    		
			    	</div>
		    	</div>
		    <?php echo $this->Form->end(); ?>
		    
	    </div>
	</div>
<div>	
    





