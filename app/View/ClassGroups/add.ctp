<div class="row">
	<div class="col-md-12">
		<div class="content-din">
		    <div class="title-page">
		    	<h1 class="title-pg">Formulário de Turmas</h1>
		    </div>

		    <?php echo $this->Form->create('ClassGroup'); ?>
		    
		    	<div class="col-md-12">
		    		<div class="form-group">
			    		<?php echo $this->Form->input('name', array('label'=>'Nome', 'class'=>'form-control')); ?>
			    	</div>
		    	</div>
		    	
		    	<div class="col-md-12">
		    		<div class="form-group">
		    		    <?php echo $this->Form->input('description', array('type'=>'textarea','label'=>'Descrição', 'class'=>'form-control','rows'=>3)); ?>
		    	    </div>
		    	</div>
		    	<div class="col-md-12">
		    		<div class="col-md-3">
			    		<div class="form-group">
			    		    <?php echo $this->Form->input('start_date', array('label'=>'Início do curso', 'class'=>'form-control', 'type' => 'text')); ?>
			    	    </div>
			    	</div>
			    	<div class="col-md-3">
			    		<div class="form-group">
			    		    <?php echo $this->Form->input('end_date', array('label'=>'Término do curso', 'class'=>'form-control', 'type' => 'text')); ?>
			    	    </div>
			    	</div>
			    	<div class="col-md-3">
			    		<div class="form-group">
			    		    <?php echo $this->Form->input('course_id', array('label'=>'Curso', 'class'=>'form-control', 'options' => $courses)); ?>
			    	    </div>
			    	</div>
			    	<div class="col-md-3">
			    		<div class="checkbox">
			    		    <?php echo $this->Form->input('status', array('label'=>'Ativar curso')); ?>
			    	    </div>
			    	</div> 
		    	</div>  	
		    	<div class="form-group">
		    		<?php echo $this->Form->button('Salvar', array('type' => 'submit', 'class'=>'btn-salvar')); ?>
		    		<?php echo $this->Form->button('Resetar', array('type' => 'reset', 'class'=>'btn-reset')); ?>
		    	</div>
		    <?php echo $this->Form->end(); ?>
		    
	    </div>
	</div>
<div>	
    

