<div class="row">
	<div class="col-md-12">
		<div class="content-din">
		    <div class="title-page">
		    	<h1 class="title-pg">Turmas</h1>
		    </div>
		    <div class="content-din-action">
		    	<a class="a-btn-insert" href=" <?php echo $this->Html->url(array("controller" => "classGroups","action" => "add"));  ?> "> <span class="glyphicon glyphicon-plus"></span> Cadastrar</a>
		    </div>
		    <?php  $paginator = $this->Paginator; // Atribui um obj de paginator a variável que vai popular nossa list?>
		    <div class="content-din-wrap">
		    	<p>Listagem de turmas</p>	
			    <table class="table table-hover users-list">
				    <thead>
				      <tr>
				        <th><?php echo $paginator->sort('name', 'Nome');?></th>
				        <th><?php echo $paginator->sort('name', 'Curso');?></th>
				        <th><?php echo $paginator->sort('start_date', 'Início');?></th>
				        <th><?php echo $paginator->sort('end_date', 'Término');?></th>
				        <th>Ação</th>
				      </tr>
				    </thead>
				    <tbody>
				     
				      

				     
				      <?php foreach($turmas as $turma): ?>
					      <tr>
					        <td><?php echo h($turma['ClassGroup']['name']); ?></td>
					         <td><?php echo h($turma['Course']['name']); ?></td>
					        <td><?php echo h($turma['ClassGroup']['start_date']); ?></td>
					        <td><?php echo h($turma['ClassGroup']['end_date']); ?></td>
					        <td> 
					            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $turma['ClassGroup']['id']),array('class' => 'btn-edit')); ?>
					            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $turma['ClassGroup']['id']), array('class'=>'btn-delete'), __('Quer mesmo deletar a turma %s?', $turma['ClassGroup']['name'])); ?>
					        </td>
					      </tr>
				      <?php endforeach; ?>
				    </tbody>
				</table>
				<?php 

					 // pagination section
				    echo "<div class='paging'>";
				 
				        // the 'first' page button
				        echo $paginator->first("Primeiro | ");
				         
				        // 'prev' page button, 
				        // we can check using the paginator hasPrev() method if there's a previous page
				        // save with the 'next' page button
				        if($paginator->hasPrev()){
				            echo $paginator->prev("Anterior | ");
				        }
				         
				        // the 'number' page buttons
				        echo $paginator->numbers(array('modulus' => 4));
				         
				        // for the 'next' button
				        if($paginator->hasNext()){
				            echo $paginator->next(" | Próximo | ");
				        }
				         
				        // the 'last' page button
				        echo $paginator->last("Último");
				     
				    echo "</div>";

				 ?>
		    </div>
		    
	    </div>
	</div>
<div>	
    

