 <div class="row">
	<div class="col-md-12">
		<div class="content-din">
		    <div class="title-page">
		    	<h1 class="title-pg">Formulário de Inscrição</h1>
		    </div>

		    <?php echo $this->Form->create('ClassGroupsUser'); ?>
		    	<?php echo $this->form->input('id', array('type'=>'hidden')); ?>
		    
		    	<div class="col-md-12">
		    		<div class="form-group">
			    		<?php echo $this->Form->input('class_group_id', array('label'=>'Turma', 'class'=>'form-control', 'options' => $turmas, 'empty' => 'Selecione a Turma')); ?>
			    	</div>
		    	</div>
		    	
		    	<div class="col-md-12">
		    		<div class="form-group">
		    		    <?php echo $this->Form->input('user_id', array('label'=>'Participante', 'class'=>'form-control', 'options' => $users, 'empty' => 'Selecione o participante')); ?>
		    	    </div>
		    	</div>
		    	<div class="col-md-12">
		    		<div class="col-md-4">
			    		<div class="form-group">
			    		    <?php echo $this->Form->input('type_enrolment_id', array('label'=>'Tipo de desconto', 'class'=>'form-control', 'options' => $descontos, 'empty' => 'Selecione a opção de desconto')); ?>
			    	    </div>
			    	</div>
			    	<div class="col-md-4">
			    		<div class="checkbox">
			    		    <?php echo $this->Form->input('status', array('label'=>'Ativar inscrição')); ?>
			    	    </div>
			    	</div>
			    	<div class="col-md-4">
			    		<div class="form-group">
				    		<?php echo $this->Form->input('price', array('label'=>'Preço', 'class'=>array('form-control','price'),'type'=>'text')); ?>
				    	</div>
			    	</div>
			    	
		    	</div>  	
		    	<div class="form-group">
		    		<?php echo $this->Form->button('Salvar', array('type' => 'submit', 'class'=>'btn-salvar')); ?>
		    		
		    	</div>
		    <?php echo $this->Form->end(); ?>
		    
	    </div>
	</div>
<div>	
    

