<div class="row">
	<div class="col-md-12">
		<div class="content-din">
		    <div class="title-page">
		    	<h1 class="title-pg">Formulário de usuários</h1>
		    </div>

		    <?php echo $this->Form->create('User'); ?>
		    	<div class="col-md-12">
		    		<div class="form-group">
			    		<?php echo $this->Form->input('name', array('label'=>'Nome', 'class'=>'form-control')); ?>
			    	</div>
		    	</div>
		    	
		    	<div class="col-md-12">
		    		<div class="form-group">
			    		<?php echo $this->Form->input('email', array('label'=>'E-mail', 'class'=>'form-control')); ?>
			    	</div>
		    	</div>
		    	<div class="col-md-12">
		    		<div class="form-group">

			    		<?php echo $this->Form->input('role', 
			    			array(
			    				'label'=>'Selecione o tipo de usuário', 
			    				'class'=>'form-control sel-user-type', 
			    				'type' => 'select',
			    				'options' => array(
			    			        'admin' => 'admin', 
									'professor' => 'professor', 
									'aluno' => 'aluno', 
									'ead' => 'ead', 
									'visitante' => 'visitante'),
    							'empty' => '(Selecione o perfil)')); 
    					?>
			    	</div>
		    	</div>
		    	<div class="col-md-12">
		    		<div class="form-group">
			    		<?php echo $this->Form->input('password', array('label'=>'Senha', 'class'=>'form-control')); ?>
			    	</div>
		    	</div>
		    	
		    	<div class="form-group">
		    		<?php echo $this->Form->button('Salvar', array('type' => 'submit', 'class'=>'btn-salvar')); ?>
		    		<?php echo $this->Form->button('Resetar', array('type' => 'reset', 'class'=>'btn-reset')); ?>
		    	</div>
		    <?php echo $this->Form->end(); ?>
		    
	    </div>
	</div>
<div>	
    

