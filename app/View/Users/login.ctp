
<div class="container">
	<div class="col-md-12">
		<div class="login-wrap">
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->Session->flash('Auth'); ?>
			<?php echo $this->Form->create('User') ?>
                <div class="form-group has-feedback ">
                     <?php echo $this->Form->input('email',array('class' => 'form-control','placeholder'=>'E-mail','label'=>false, 'div'=>false)); ?>
                    <span class="fa fa-at form-icon-input"></span>
                                    </div>
                <div class="form-group has-feedback ">
                	<?php echo $this->Form->input('password',array('class' => 'form-control','placeholder'=>'Senha','label'=>false, 'div'=>false)); ?>
                    
                    <span class="fa fa-key form-icon-input"></span>
                                    </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label class="">
                                <div class="" style="position: relative;" aria-checked="false" aria-disabled="false"><input name="remember" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> Lembrar-me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                    	<?php echo $this->Form->submit('Enviar', array('class' => 'btn-salvar')); ?>
                        
                    </div>
                    <!-- /.col -->
                </div>
            <?php $this->Form->end(); ?>
		</div>
	</div>
</div>

