<div class="row">
	<div class="col-md-12">
		<div class="content-din">
		    <div class="title-page">
		    	<h1 class="title-pg">Formulário de cursos</h1>
		    </div>

		    <?php echo $this->Form->create('Course'); ?>
		        <?php echo $this->form->input('id', array('type'=>'hidden')); ?>
		    	<div class="class-md-12">
		    		<div class="form-group">
		    		<?php echo $this->Form->input('name', array('label'=>'Nome', 'class'=>'form-control')); ?>
		    	</div>
		    	</div>
		    	
		    	<div class="col-md-6">
		    		<div class="form-group">
		    		    <?php echo $this->Form->input('description', array('type'=>'textarea','label'=>'Descrição', 'class'=>'form-control','rows'=>3)); ?>
		    	    </div>
		    	</div>
		    	<div class="col-md-6">
		    		<div class="form-group">
		    		    <?php echo $this->Form->input('notice', array('label'=>'Observação', 'class'=>'form-control','type'=>'textarea','rows'=> 3)); ?>
		    	    </div>
		    	</div>
		    	<div class="col-md-6">
		    		<div class="form-group">
		    		
		    		<?php 
		    			echo $this->Form->label('', 'Escolha a modaldade de esnsino').'<br />';
		    			$options = array(1 => 'Presencial', 2 => 'EAD', 3 => 'Semi-Presencial' );
						$attributes = array('legend' => false,'style'=>'margin-left:10px;');
						echo $this->Form->radio('modality', $options, $attributes);
		    		?>
		    	    </div>
		    	</div>
		    	<div class="col-md-6">
		    	    <div class="row">
		    	    	<div class="col-md-6"></div>
		    	    	<div class="col-md-6">
		    	    		<div class="form-group">
					    		<?php echo $this->Form->input('price', array('label'=>'Preço', 'class'=>array('form-control','price'),'type'=>'text')); ?>
					    	</div>
		    	    	</div>
		    	    </div>
		    	</div>
		    	
		    	
		    	
		    	<div class="form-group">
		    		<?php echo $this->Form->button('Salvar', array('type' => 'submit', 'class'=>'btn-salvar')); ?>
		    		
		    	</div>
		    <?php echo $this->Form->end(); ?>
		    
	    </div>
	</div>
<div>	
