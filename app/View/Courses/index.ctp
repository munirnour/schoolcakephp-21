<div class="row">
	<div class="col-md-12">
		<div class="content-din">
		    <div class="title-page">
		    	<h1 class="title-pg">Cursos</h1>
		    </div>
		    <div class="content-din-action">
		    	<a class="a-btn-insert" href=" <?php echo $this->Html->url(array("controller" => "courses","action" => "add"));  ?> "> <span class="glyphicon glyphicon-plus"></span> Cadastrar</a>
		    </div>
		    <?php  $paginator = $this->Paginator; // Atribui um obj de paginator a variável que vai popular nossa list?>
		    <div class="content-din-wrap">
		    	<p>Lista de todos os cursos já cadastrados</p>	
			    <table class="table table-hover courses-list">
				    <thead>
				      <tr>
				        <th><?php echo $paginator->sort('name', 'Nome');?></th>
				        <th><?php echo $paginator->sort('modality', 'Modalidade');?></th>
				        <th><?php echo $paginator->sort('price', 'Preço');?></th>
				        <th>Ação</th>
				      </tr>
				    </thead>
				    <tbody>
				     
				      <?php 
				      	
				         $modalidade = array(0 => '', 1 => 'Presencial', 2 => 'Online', 3 => 'Semi Presencial'); 
				        

				      ?>

				     
				      <?php foreach($courses as $course): ?>
					      <tr>
					        <td><?php echo h($course['Course']['name']); ?></td>
					        <td><?php echo h($modalidade[$course['Course']['modality']]); ?></td>
					        <td><?php echo h($course['Course']['price']); ?></td>
					        <td> 
					            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $course['Course']['id']),array('class' => 'btn-edit')); ?>
					            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $course['Course']['id']), array('class'=>'btn-delete'), __('Tu queres apgar o curso %s?', $course['Course']['name'])); ?>
					        </td>
					      </tr>
				      <?php endforeach; ?>
				    </tbody>
				</table>
				<?php 

					 // pagination section
				    echo "<div class='paging'>";
				 
				        // the 'first' page button
				        echo $paginator->first("Primeiro | ");
				         
				        // 'prev' page button, 
				        // we can check using the paginator hasPrev() method if there's a previous page
				        // save with the 'next' page button
				        if($paginator->hasPrev()){
				            echo $paginator->prev("Anterior | ");
				        }
				         
				        // the 'number' page buttons
				        echo $paginator->numbers(array('modulus' => 4));
				         
				        // for the 'next' button
				        if($paginator->hasNext()){
				            echo $paginator->next(" | Próximo | ");
				        }
				         
				        // the 'last' page button
				        echo $paginator->last("Último");
				     
				    echo "</div>";

				 ?>
		    </div>
		    
	    </div>
	</div>
<div>	
    

