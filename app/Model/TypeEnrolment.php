<?php
App::uses('AppModel','Model');

class TypeEnrolment extends AppModel{

	public $validate = array(
		'name' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Campo nome não foi preenchido.'
			),
		'description' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Campo modalidade não foi preenchido.'
			)
	);
}