<?php
App::uses('AppModel','Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel{
	var $name = "User";
	public $validate = array(
		'name' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Campo nome não foi preenchido.'
			),
		'role' => array(
            'valid' => array(
                'rule' => array('inList', array('admin', 'professor', 'aluno', 'ead', 'visitante')),
                'message' => 'Função não definada, por favor selecione novamente!',
                'allowEmpty' => false
            )
        ),
		
		'email' => array(
				'rule' => array(
						'email'
					),
				'message' => 'Campo email não foi preenchido.'
			),
		'password' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Campo senha não foi preenchido.'
			)
	);

	public $hasAndBelongsToMany = array(
        'ClassGroup' =>
            array(
                'className' => 'ClassGroup',
                'joinTable' => 'class_groups_users',
                'foreignKey' => 'user_id',
                'associationForeignKey' => 'class_group_id'
            )
    );

	public function beforeSave($options = array()) {
	    if (isset($this->data['User']['password'])) {
	        $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
	    }
	    return true;
	}

}