<?php
App::uses('AppModel','Model');

class ClassGroup extends AppModel{

	var $name = "ClassGroup";

	public $validate = array(
		'name' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Insere um nome para este curso!'
			),
		'description' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Insere uma descrição para esta turma!'
			),
		
		'start_date' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Informe a data de início!'
			),
		'end_date' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Informe a data de término!'
			),
		'course_id' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Precisa selecionar um curso!'
			),
		'status' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Defina o status da turma!'
			)
	);

	public $belongsTo = array('Course');

    public $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'class_groups_users',
			'foreignKey' => 'class_group_id',
			'associationForeignKey' => 'user_id'
		),
	);

	

	
}