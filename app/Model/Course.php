<?php
App::uses('AppModel','Model');

class Course extends AppModel{

	public $validate = array(
		'name' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Campo nome não foi preenchido.'
			),
		'modality' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Campo modalidade não foi preenchido.'
			),
		
		'price' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Campo valor não foi preenchido.'
			)
	);

	public $hasMany = array('ClassGroup');
}