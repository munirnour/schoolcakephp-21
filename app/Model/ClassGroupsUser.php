<?php
App::uses('AppModel','Model');

class ClassGroupsUser extends AppModel{
	public $validate = array(
		'class_group_id' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Selecione a turma!'
			),
		'user_id' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Selecione o aluno!'
			),
		
		'type_enrolment_id' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Informe o tipo de desconto'
			),
		'price' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Informe o valor com desconto!'
			),
		'status' => array(
				'rule' => array(
						'notBlank'
					),
				'message' => 'Defina o status da matrícula!'
			)
	);


	//public $hasMany = array('ClassGroup');
	
	public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'ClassGroup' => array(
        	'className' => 'ClassGroup',
        	'foreignKey' => 'class_group_id'
        )

    );

	
}