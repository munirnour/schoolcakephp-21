	  

$(document).ready(function(){

  $(".price").mask('0.000,00', {reverse: true});
  $('.courses-list tr td:nth-child(3)').mask('0.000,00', {reverse: true});
  $('.type-enrolments-list tr td:nth-child(3)').mask('0.000,00', {reverse: true});
  $('.type-enrolments-list tr td:nth-child(2)').mask('0.000,00', {reverse: true});

  // Calendário datepicker
  
  /*Turmas cadastro*/
  $('#ClassGroupStartDate').datepicker();
  $('#ClassGroupEndDate').datepicker();
   
  // Inscrições Ajax Métodos
  
  $("#ClassGroupsUserClassGroupId").change(function(){  
    var sel = $(this);               
    var cod_turma = $("#ClassGroupsUserClassGroupId option:selected").val();
    
    
    

    if(cod_turma !== ""){

    	$.ajax({
	        url :'/school/ClassGroupsUsers/getCoursePrice/',
	        dataType :'json',
	        type: "Post",
	        data: {turma:cod_turma},
	        success:function(data) {
	        	$("#ClassGroupsUserPrice").val(data['price']);
	            
	        },
	        error : function(){
	        	alert(e);
	        	console(e);
	        } 
	    });
    	

        
    }else{
    	alert('Erro!');
    }

    
    }); 
   


  
});