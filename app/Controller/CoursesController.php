<?php

App::uses('AppController', 'Controller');

class CoursesController extends AppController{

	
	public $components = array('Paginator');


	public function index(){
		$this->Course->recursive = 0;
		$this->paginate = array(
			'condiction' => array('Course.id !=' => '5'),
			'limit' => 10 ,
			'order' => array('id'=>'desc')
			
		);
		$this->set('courses', $this->Paginator->paginate());
	}

	public function add(){
		if($this->request->is('post')){
			$this->Course->create();

			
			//debug($this->request->data) or die();
			$valor = str_replace("." , "" , $this->request->data['Course']['price'] ); // Primeiro tira os pontos
	        $valor = str_replace("," , "." , $valor); // Depois substitua a vírgula pelo ponto
	        $x_val = number_format((float) $valor,2,'.',''); 
            $this->request->data['Course']['price'] = $x_val;
            echo $this->request->data['Course']['modality'];
			if($this->Course->save($this->request->data)){
				$this->Session->setFlash('Curso foi salvo!');
				
				return $this->redirect(array('action'=> 'index'));
			}

		}
	}

	public function edit($id = null){

		if(!$id){
			throw new NotFoundException(__('Curso inválido!'));
		}

		$course = $this->Course->findById($id);
		if(!$course){
			throw new NotFoundException(__('Curso inválido!'));
		}
		if($this->request->is(array('post','put'))){
			$this->Course->id = $id;
			$valor = str_replace("." , "" , $this->request->data['Course']['price'] ); // Primeiro tira os pontos
	        $valor = str_replace("," , "." , $valor); // Depois substitua a vírgula pelo ponto
	        $x_val = number_format((float) $valor,2,'.','');

	        $this->request->data['Course']['price'] = $x_val; 
			
			if($this->Course->save($this->request->data)){
				$this->Session->setFlash('Curso atualizado com sucesso!');
				return $this->redirect(array('action'=>'index'));
			}

			$this->Session->setFlash('As modificações não foram atualizadas!');
		}

		$this->request->data = $this->Course->findById($id);
	}

	public function delete($id){
		if($this->request->is('get')){
			throw new MethodNotAllowedException();
		}

		if($this->Course->delete($id)){
			$this->Session->setFlash(__('O curso foi apagado com sucesso!'));
		}else{
			$this->Session->setFlash(__('O curso não pode ser apagado!'));
		}
		return $this->redirect(array('action'=>'index'));

	}


	

}