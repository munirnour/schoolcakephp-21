<?php

App::uses('AppController', 'Controller');

class ClassGroupsController extends AppController{

	
	public $components = array('Paginator');


	public function index(){
		//$this->ClassGroup->recursive = 0;
		$this->paginate = array(
			'limit' => 10 ,
			'order' => array('id'=>'desc')
			
		);
		$this->set('turmas', $this->Paginator->paginate());
	}

	private function getCourses(){
		$courses = $this->ClassGroup->Course->find('list', array('fields' => array('id','name')));
		$this->set(compact('courses'));
	}

	
	public function view($id = null){

	}


	public function add(){
		if($this->request->is('post')){
			$this->ClassGroup->create();
			$this->request->data['ClassGroup']['start_date'] = date("Y-m-d 00:00:00", strtotime($this->request->data['ClassGroup']['start_date']));
			$this->request->data['ClassGroup']['end_date'] = date("Y-m-d 00:00:00", strtotime($this->request->data['ClassGroup']['end_date']));
			

			if($this->ClassGroup->save($this->request->data)){
				$this->Session->setFlash('A turma foi criada com sucesso!');
							
				return $this->redirect(array('action'=> 'index'));
			}	
		}
		self::getCourses();
	}

	public function edit($id = null){

		if(!$id){
			throw new NotFoundException(__('Turma inexistente!'));
		}

		$turma = $this->ClassGroup->findById($id);
		if(!$turma){
			throw new NotFoundException(__('Turma inválida!'));
		}
		if($this->request->is(array('post','put'))){
			$this->ClassGroup->id = $id;
			 
			if($this->ClassGroup->save($this->request->data)){
				$this->Session->setFlash('Turma atualizada com sucesso!');
				return $this->redirect(array('action'=>'index'));
			}

			$this->Session->setFlash('As modificações não foram atualizadas!');
		}
		self::getCourses();
		$this->request->data = $this->ClassGroup->findById($id);
	}

	public function delete($id){
		if($this->request->is('get')){
			throw new MethodNotAllowedException();
		}

		if($this->ClassGroup->delete($id)){
			$this->Session->setFlash(__('A turma foi apagada com sucesso!'));
		}else{
			$this->Session->setFlash(__('A turma não pode ser apagado!'));
		}
		return $this->redirect(array('action'=>'index'));

	}





	

}