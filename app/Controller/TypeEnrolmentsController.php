<?php

App::uses('AppController', 'Controller');

class TypeEnrolmentsController extends AppController{

	
	public $components = array('Paginator');


	public function index(){
		$this->TypeEnrolment->recursive = 0;
		$this->paginate = array(
			'condiction' => array('TypeEnrolment.id !=' => '5'),
			'limit' => 10 ,
			'order' => array('id'=>'desc')
			
		);
		$this->set('type_enrolments', $this->Paginator->paginate());
	}
    
    public function changeCoin($value = null){
    	$valor = str_replace("." , "" , $value ); // Primeiro tira os pontos
	        $valor = str_replace("," , "." , $valor); // Depois substitua a vírgula pelo ponto
	        return number_format((float) $valor,2,'.',''); 
    }
	public function add(){
		if($this->request->is('post')){
			$this->TypeEnrolment->create(); 
			//debug($this->request->data) or die();
	        $x_val = $this->changeCoin($this->request->data['TypeEnrolment']['discount']);
	        $y_val = $this->changeCoin($this->request->data['TypeEnrolment']['amount']);
            $this->request->data['TypeEnrolment']['discount'] = $x_val;
            $this->request->data['TypeEnrolment']['amount'] = $y_val;
			if($this->TypeEnrolment->save($this->request->data)){
				$this->Session->setFlash('Tipo de inscriçao foi salvo!');
				
				return $this->redirect(array('action'=> 'index'));
			}

		}
	}

	public function edit($id = null){

		if(!$id){
			throw new NotFoundException(__('Curso inválido!'));
		}

		$typeenrolment = $this->TypeEnrolment->findById($id);
		if(!$typeenrolment){
			throw new NotFoundException(__('Tipo de inscrição inválida!'));
		}
		if($this->request->is(array('post','put'))){
			$this->TypeEnrolment->id = $id;
			$x_val = $this->changeCoin($this->request->data['TypeEnrolment']['discount']);
	        $y_val = $this->changeCoin($this->request->data['TypeEnrolment']['amount']);
            $this->request->data['TypeEnrolment']['discount'] = $x_val;
            $this->request->data['TypeEnrolment']['amount'] = $y_val; 
			
			if($this->TypeEnrolment->save($this->request->data)){
				$this->Session->setFlash('Tipo de inscrição atualizado com sucesso!');
				return $this->redirect(array('action'=>'index'));
			}

			$this->Session->setFlash('As modificações não foram atualizadas!');
		}

		$this->request->data = $this->TypeEnrolment->findById($id);
	}

	public function delete($id){
		if($this->request->is('get')){
			throw new MethodNotAllowedException();
		}

		if($this->TypeEnrolment->delete($id)){
			$this->Session->setFlash(__('O tipo de inscrição foi apagado com sucesso!'));
		}else{
			$this->Session->setFlash(__('O tipo de inscrição não pode ser apagado!'));
		}
		return $this->redirect(array('action'=>'index'));

	}


	

}