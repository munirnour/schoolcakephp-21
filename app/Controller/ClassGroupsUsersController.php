<?php
App::uses('AppController','Controller');

class ClassGroupsUsersController extends AppController{
	public $components = array('Paginator','RequestHandler');


	public function index(){
		$this->ClassGroupsUser->recursive = 0;
		$this->paginate = array(
			'limit' => 10 ,
			'order' => array('id'=>'desc')
			
		);
		$this->set('inscricoes', $this->Paginator->paginate());
	}

	private function getUsers(){
		$this->loadModel('User');
		$users = $this->User->find('list', array('fields' => array('id','name')));
		$this->set(compact('users'));
	}

	private function getClassGroups(){
		$this->loadModel('ClassGroup');
		$turmas = $this->ClassGroup->find('list', array('fields' => array('id', 'name')));
		$this->set(compact('turmas'));
	}

	private function getEnrolments(){
		$this->loadModel('TypeEnrolment');
		$descontos = $this->TypeEnrolment->find('list', array('fields' => array('id','name')));
		$this->set(compact('descontos'));
	}

	public function getCoursePrice(){

		if($this->request->is('ajax')){
			$this->autoRender = false;
			$this->request->allowMethod(array('ajax'));
			$turma = $this->request->data['turma'];

			$this->loadModel('ClassGroup');
			$turma = $this->ClassGroup->findById(2);
			$data = array('price'=>$turma['Course']['price']);
		
			return json_encode($data);
		}
		
	}

	public function teste(){

		debug(RequestHandler::getAjaxVersion());
		$this->ClassGroupsUser->recursive = 0;
		$resultado = $this->ClassGroupsUser->User->find('all');
		$this->loadModel('User');
		$result2 = $this->User->find('list',array('fields'=>array('User.name','User.id')));

		//debug($result);
		//echo 'Saída de dados: '. $resultado['User']['name'];
	}
	

	public function view($id = null){

	}

	public function add(){
		if($this->request->is('post')){
			
			$this->ClassGroupsUser->create();
			if($this->ClassGroupsUser->save($this->request->data)){
				$this->Session->setFlash('A inscrição foi criada com sucesso!');
							
				return $this->redirect(array('action'=> 'index'));
			}	
		}

		self::getUsers();
		self::getClassGroups();
		self::getEnrolments();

		
	}

	public function edit($id = null){
		//debug($this->ClassGroupsUser->findById($id));
		if(!$id){
			throw new NotFoundException(__('Inscrição inexistente!'));
		}

		$inscricao = $this->ClassGroupsUser->findById($id);
		if(!$inscricao){
			throw new NotFoundException(__('Incrição inválida!'));
		}
		if($this->request->is(array('post','put'))){
			$this->ClassGroupsUser->id = $id;

			//debug($this->request->data);
			$valor = str_replace("." , "" , $this->request->data['ClassGroupsUser']['price'] ); 
	        $valor1 = str_replace("," , "." , $valor);
	        $x_val = number_format((float) $valor1,2,'.','');
			$this->request->data['ClassGroupsUser']['price'] = $x_val;
			 
			if($this->ClassGroupsUser->save($this->request->data)){
				$this->Session->setFlash('Inscrição atualizada com sucesso!');
				return $this->redirect(array('action'=>'index'));
			}

			$this->Session->setFlash('As modificações não foram atualizadas!');
		}
		self::getUsers();
		self::getClassGroups();
		self::getEnrolments();
		$this->request->data = $this->ClassGroupsUser->findById($id);
	}

	public function delete($id){
		if($this->request->is('get')){
			throw new MethodNotAllowedException();
		}

		if($this->ClassGroupsUser->delete($id)){
			$this->Session->setFlash(__('A inscrição foi apagada com sucesso!'));
		}else{
			$this->Session->setFlash(__('A inscrição não pode ser apagado!'));
		}
		return $this->redirect(array('action'=>'index'));

	}
}