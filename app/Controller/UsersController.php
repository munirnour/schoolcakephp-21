<?php

App::uses('AppController', 'Controller');


class UsersController extends AppController{

	
	public $components = array('Paginator');

	
	public function index(){
		$this->User->recursive = 0;
		$this->paginate = array(
			'limit' => 10 ,
			'order' => array('id'=>'desc')
			
		);
		$this->set('users', $this->Paginator->paginate());
	}

	public function add(){
		if($this->request->is('post')){
			$this->User->create();
			//debug($this->request->data) or die();
			if($this->User->save($this->request->data)){
				$this->Session->setFlash('Usuário foi salvo!');
				
				return $this->redirect(array('action'=> 'index'));
			}

		}
	}

	public function edit($id = null){

		if(!$id){
			throw new NotFoundException(__('Usuário inválido!'));
		}

		$user = $this->User->findById($id);
		if(!$user){
			throw new NotFoundException(__('Usuário inválido!'));
		}
		if($this->request->is(array('post','put'))){
			$this->User->id = $id; 
			
			if($this->User->save($this->request->data)){
				$this->Session->setFlash('Usuário atualizado com sucesso!');
				return $this->redirect(array('action'=>'index'));
			}

			$this->Session->setFlash('As modificações não foram atualizadas!');
		}
		unset($this->request->data['User']['password']);
		$this->request->data = $this->User->findById($id);
	}

	public function delete($id){
		if($this->request->is('get')){
			throw new MethodNotAllowedException();
		}

		if($this->User->delete($id)){
			$this->Session->setFlash(__('O usuário foi apagado com sucesso!'));
		}else{
			$this->Session->setFlash(__('O usuário não pode ser apagado!'));
		}
		return $this->redirect(array('action'=>'index'));

	}

	public function login(){
		$this->layout = 'login';
		//debug($this->request->data);
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->redirectUrl());
			}

			$this->Session->setFlash('Ops, não foi autorizado suas credenciais, por favor tente novamente!');
		}
		
	}

	

	public function logout() {
		return $this->redirect($this->Auth->logout());
	}



	

}